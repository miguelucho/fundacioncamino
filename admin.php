<?php
	session_start();
	if(isset($_SESSION['usulogfunda'])){
		header('Location: home.php');
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<meta name="keywords" lang="es" content="">
<meta name="robots" content="All">
<meta name="description" lang="es" content="">
<title>Fundación Camino de la Esperanza</title>
<link rel="stylesheet" href="css/stylesheet.css" />
<link rel="stylesheet" href="css/sweetalert2.css" />
<link rel="stylesheet" href="css/msj.css" />
</head>
<body class="BG">
<div class="Login">
	<div class="Login-int">
		<div class="Login-int-form">
			<h2>Iniciar Sesión</h2>
			<p>Ingresa la información para iniciar sesión</p>
			<form id="logueo">
				<label>Usuario</label>
				<input type="text" placeholder="Usuario" name="nombre" required="">
				<label>Contraseña</label>
				<input type="password" placeholder="Contraseña" name="password" required="">
				<label>Verifica que la información sea correcta.</label>
				<input type="Submit" class="Btn-naranja" style="height: 40px;" value="iniciar sesión">
			</form>
		</div>
	</div>
</div>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/deslizar.js"></script>
<script src="js/menu-mini.js" type="text/javascript"></script>
<script src="js/sweetalert2.min.js"></script>
<script type="text/javascript" src="js/script_login.js"></script>
</body>
</html>
