$(function() {
    $('a[href*=#]').click(function() {

        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
            if ($target.length) {
                var targetOffset = $target.offset().top;
                $('html,body').animate({
                    scrollTop: targetOffset
                }, 1000);
                return false;
            }
        }
    });

    $('#l-productos li').on('click', function() {
        mostrar = $(this);
        switch (mostrar.index()) {
            case 0:
                $('#pbx, #ha, #cc, #seg').fadeOut(0, function() {
                    $('#ivr').fadeIn(0, function() {
                        $('#productos-info').animate({
                            scrollTop: $('#ivr')
                        }, 0);
                    });
                });
                break;
            case 1:
                $('#ivr, #ha, #cc, #seg').fadeOut(0, function() {
                    $('#pbx').fadeIn(0, function() {
                        $('#productos-info').animate({
                            scrollTop: $('#pbx')
                        }, 0);
                    });
                });
                break;
            case 2:
                $('#ivr, #pbx, #cc, #seg').fadeOut(0, function() {
                    $('#ha').fadeIn(0, function() {
                        $('#productos-info').animate({
                            scrollTop: $('#ha')
                        }, 0);
                    });
                });
                break;
            case 3:
                $('#ivr, #pbx, #ha, #seg').fadeOut(0, function() {
                    $('#cc').fadeIn(0, function() {
                        $('#productos-info').animate({
                            scrollTop: $('#cc')
                        }, 0);
                    });
                });
                break;
            case 4:
                $('#ivr, #pbx, #ha, #cc').fadeOut(0, function() {
                    $('#seg').fadeIn(0, function() {
                        $('#productos-info').animate({
                            scrollTop: $('#seg')
                        }, 0);
                    });
                });
                break;
        }
    });
});