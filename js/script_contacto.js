$(document).ready(function(){

	$('#contacto').on('submit',function(e){
		e.preventDefault();
		swal({
		    html: $("<div>")
		          .addClass('loader')
		          .text('Enviando...'),
		    background: 'rgba(255, 255, 255, 0)',
		    showConfirmButton: false,
		    allowOutsideClick: false,
		    allowEscapeKey: false
		});
		data = $(this).serializeArray();
		$.post('libs/correo.php',data).done(function(data){
			if(data == 'correcto'){
				swal({
				    title: "",
				    text: "Su mensaje ha sido enviado!",
				    type: "success",
				    timer: 2000,
				    showConfirmButton: false,
				    allowOutsideClick: false,
		    		allowEscapeKey: false
				});
				$('input,textarea').not('input[type=submit]').val('');
			}
			else if(data == 'error'){
				swal({
				    title: "Error!",
				    text: "Su mensaje no ha podido ser enviado!",
				    type: "error",
				    timer: 3000,
				    showConfirmButton: false,
				    allowOutsideClick: false,
		    		allowEscapeKey: false
				});
			}
		});
	});

});