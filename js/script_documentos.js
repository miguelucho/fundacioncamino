$(document).ready(function() {
    cargardocs();


    function cargardocs() {
        $.getJSON('libs/acc_documentos', {
            opc: 'cargardocs'
        }).done(function(data) {
            $('#ls-documentos').empty();
            $.each(data.documentos, function(i, dat) {
                $('#ls-documentos').append('<div class="Listar-table">' +
                    '<div class="Listar-table-dato Izqui">' +
                    '<span class="" title="">' + dat.nombre + '</span>' +
                    '</div>' +
                    '<div class="Listar-table-dato">' +
                    '<span class="" title=""><a href="javascript://" target="_blank" class="Btn-naranja borrar" id="' + dat.id + '">Borrar</a></span>' +
                    '</div>' +
                    '</div>');
            });
        });
    }

    $('body').on('click', '.borrar', function() {
        registro = $(this);
        modal({
            type: 'confirm',
            title: 'Eliminar Documento',
            text: 'Desea eliminar el Documento ?',
            size: 'small',
            callback: function(result) {
                if (result === true) {
                    id = registro.prop('id');
                    registro.parent().parent().closest('.Listar-table').fadeOut(500, function() {
                        $(this).remove();
                    });
                    $.post('libs/acc_documentos.php', {
                        opc: 'borrar',
                        id: id
                    }, 'json');
                }
            },
            closeClick: false,
            theme: 'xenon',
            animate: true,
            buttonText: {
                yes: 'Confirmar',
                cancel: 'Cancelar'
            }
        });
    });


    $('#crea').on('submit', function(e) {
        e.preventDefault();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append("<div style='position: absolute;top: 50%;left: 50%;'><img src='images/esperar.gif'></div>");
        setTimeout(function() {
            $('#fondo').fadeIn('fast', function() {
                $('#rp').fadeIn();
            });
        }, 400);
        var inputFileImage = document.getElementById('adjunto');
        var file = inputFileImage.files[0];
        var data = new FormData();
        data.append('archivo', file);
        data.append('opc', 'crear');
        var otradata = $('#crea').serializeArray();
        $.each(otradata, function(key, input) {
            data.append(input.name, input.value);
        });
        var url = 'libs/acc_documentos';
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,
            dataType: 'json',
            success: function(data) {
                if (data == true) {
                    $('#editar').click();
                    $('#fondo').remove();
                    $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                    $('#fondo').append("<div class='rp' style='display: none; text-align: center' id='rp'><span>Documento Agregado</span></div>");
                    setTimeout(function() {
                        $('#fondo').fadeIn('fast', function() {
                            $('#rp').animate({
                                'top': '350px'
                            }, 50).fadeIn();
                        });
                    }, 400);
                    setTimeout(function() {
                        $("#rp").fadeOut();
                        $('#fondo').fadeOut('fast');
                    }, 2000);
                    $(':input').not('[type=submit]').val('');
                    cargardocs();
                } else {
                    $('#fondo').remove();
                    $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                    $('#fondo').append("<div class='rp' style='display: none; text-align: center; background: #ff5643' id='rp'><span>Error! El documento no se puedo guardar.</span></div>");
                    setTimeout(function() {
                        $('#fondo').fadeIn('fast', function() {
                            $('#rp').animate({
                                'top': '350px'
                            }, 50).fadeIn();
                        });
                    }, 400);
                    setTimeout(function() {
                        $("#rp").fadeOut();
                        $('#fondo').fadeOut('fast');
                    }, 2000);
                }
            }
        });
    });


});