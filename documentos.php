<?php
	require('libs/conexion.php');
	$ls_documentos = '';
	$documentos = mysqli_query($conexion,"SELECT nombre,archivo FROM documentos ORDER BY id DESC");
	while($rsd = mysqli_fetch_object($documentos)){
		$ls_documentos .= '<div class="Listar-table">
								<div class="Listar-table-dato Izqui">
								<span class="" title="'.$rsd->nombre.'">'.$rsd->nombre.'</span>
							</div>
							<div class="Listar-table-dato">
								<span class="" title=""><a href="'.$rsd->archivo.'" target="_blank" class="Btn-naranja">Descargar</a></span>
							</div>
						</div>';
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<meta name="keywords" lang="es" content="">
<meta name="robots" content="All">
<meta name="description" lang="es" content="">
<title>Fundación Camino de la Esperanza</title>
<link rel="stylesheet" href="css/stylesheet.css" />
<link rel="stylesheet" href="css/sweetalert2.css" />
</head>
<body>
<header>
	<div class="Top">
		<div class="Top-int">
			<div class="Top-int-top">
				<a href="/"><h1>Fundación Camino de la Esperanza</h1></a>
			</div>
			<div id="Drop">
					<a href="#">Menú</a>
			</div>
			<div class="Top-int-bottom">
				<nav>
					<ul>
						<li><a href="/">Inicio</a></li>
						<li><a href="/#nosotros">Nosotros</a></li>
						<li><a href="/#programas">Programas</a></li>
						<li><a href="documentos">Documentos</a></li>
						<li><a href="/#contactenos">Contáctenos</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>
<section>
	<div class="Documentos">
		<div class="Documentos-int">
			<h1>Documentación</h1>
			<div class="Documentos-int-table">
				<div class="Listar-table Oculto">
					<div class="Listar-table-dato Izqui">
						<span class="Title">Nombre</span>
					</div>
					<div class="Listar-table-dato">
						<span class="Title">Descargar</span>
					</div>
				</div>

				<div style="display: block;">
					<?php echo $ls_documentos ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="Contactenos">
		<?php include_once("mod-correo.php") ?>
	</div>
</section>
<footer>
	<div class="Foot">
		<div class="Foot-int">
			<div class="Foot-int-sec">
				<h2>Sede Administrativa</h2>
				<p>Dir. Calle 12 No. 47A 11 Barrio La Esperanza 1ra. Etapa<br>
				Tel. 667 44 35 – 668 33 36<br>
				Cel. 311 854 38 19</p>
				<h2>Sede San José - La Reliquia</h2>
				<p>Dir. Mz. 76 Casa 9 B. La Reliquia<br>
				Tel. 683 0294<br>
				Cel. 312 523 21 31</p>
			</div>
			<div class="Foot-int-sec">
				<h2>Sede Centro de Capacitación Microempresarial Padre José Otter</h2>
				<p>Dir. Cll 30 N 26-56 B. Porvenir<br>
				Tel. 667 8210<br>
				Cel. 314 393 67 00</p>
				<h2>Sede Centro Sagrada Familia</h2>
				<p>Dir. Cll 52 Sur N 43-81 c. Porfía<br>
				Tel. 669 54 18<br>
				Cel. 311 854 48 16</p>
			</div>
			<div class="Foot-int-sec">
				<h2>Aldea Estudiantil con modalidad de internado</h2>
				<p>Granja San José - Vereda Caney Medio - Cumaral Meta<br>
				Cel. 310 202 69 68</p>
			</div>
		</div>
		<div class="Foot-int Bord">
			<p>Copyright © Fundación Camino de la Esperanza, 2018. All rights reserved Designed by <a href="http://www.inngeniate.com" target="_blank">Inngeniate.com</a></p>
		</div>
	</div>
</footer>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/deslizar.js"></script>
<script src="js/menu-mini.js" type="text/javascript"></script>
<script src="js/sweetalert2.min.js"></script>
<script type="text/javascript" src="js/script_contacto.js"></script>
</body>
</html>
