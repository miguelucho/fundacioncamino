<?php
	session_start();
	if(!isset($_SESSION['usulogfunda'])){
		header('Location: admin.php');
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<meta name="keywords" lang="es" content="">
<meta name="robots" content="All">
<meta name="description" lang="es" content="">
<title>Fundación Camino de la Esperanza</title>
<link rel="stylesheet" href="css/stylesheet.css" />
<link rel="stylesheet" href="css/sweetalert2.css" />
<link rel="stylesheet" href="css/msj.css" />
<link rel="stylesheet" href="css/jquery.modal.css" />
<link rel="stylesheet" href="css/jquery.modal.theme-xenon.css" />
</head>
<body class="BG">
<div class="Login">
<header>
	<div class="Top">
		<div class="Top-int">
			<div class="Top-int-top">
				<a href="/"><h1>Fundación Camino de la Esperanza</h1></a>
			</div>
			<div id="Drop">
					<a href="#">Menú</a>
			</div>
			<div class="Top-int-bottom">
				<nav>
					<ul>
						<li><a href="home">Documentos</a></li>
						<li><a href="libs/logout">Cerrar sesión</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>
	<div class="Login-int">
		<div class="Home-docs">
			<div class="Home-docs-int">
				<div class="Home-docs-int-form">
					<p>Ingresa el nombre y adjunta el archivo a guardar.</p>
					<form id="crea">
						<label>Nombre del archivo</label>
						<input type="text" placeholder="Nombre" name="nombre" required="true">
						<label>Selecciona el archivo a adjuntar</label>
						<input type="file" placeholder="Archivo" id="adjunto" required="true">
						<p></p>
						<label>Verifica que la información sea correcta</label>
						<input type="submit" class="Btn-naranja" style="height: 40px;" value="Guardar">
					</form>
				</div>
				<p><br></p>
				<h2>Documentos</h2>
				<div class="Documentos-int-table">
				<div class="Listar-table Oculto">
					<div class="Listar-table-dato Izqui">
						<span class="Title">Nombre</span>
					</div>
					<div class="Listar-table-dato">
						<span class="Title">Descargar</span>
					</div>
				</div>

				<div style="display: block;" id="ls-documentos">
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/deslizar.js"></script>
<script src="js/menu-mini.js" type="text/javascript"></script>
<script src="js/sweetalert2.min.js"></script>
<script type="text/javascript" src="js/script_documentos.js"></script>
<script type="text/javascript" src="js/jquery.modal.min.js"></script>
</body>
</html>
