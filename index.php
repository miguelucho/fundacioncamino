<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<meta name="keywords" lang="es" content="">
<meta name="robots" content="All">
<meta name="description" lang="es" content="">
<title>Fundación Camino de la Esperanza</title>
<link rel="stylesheet" href="css/stylesheet.css" />
<link rel="stylesheet" href="css/sweetalert2.css" />
</head>
<body>
<header>
	<div class="Top">
		<div class="Top-int">
			<div class="Top-int-top">
				<a href="/"><h1>Fundación Camino de la Esperanza</h1></a>
			</div>
			<div id="Drop">
					<a href="#">Menú</a>
			</div>
			<div class="Top-int-bottom">				
				<nav>
					<ul>
						<li><a href="/">Inicio</a></li>
						<li><a href="#nosotros">Nosotros</a></li>
						<li><a href="#programas">Programas</a></li>
						<li><a href="documentos">Documentos</a></li>
						<li><a href="#contactenos">Contáctenos</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>

<section>
	<div class="Imagen-fondo">
		<div class="Imagen-fondo-int">
			<div class="Imagen-logo">
				<img src="images/logo.png">
			</div>
		</div>
	</div>

</section>
<section>
	<div class="Contenedor">
		<div class="Contenedor-int">
			
			<h2>Camino de la Esperanza - Weg der Hoffnung</h2>
			<h3>FUNDACION CAMINO DE LA ESPERANZA </h3>
			<h3>25 AÑOS</h3>
			<h3>1993 - 2018</h3>
			<h3></h3>
			<br>		
			<p>A ejemplo de nuestros Fundadores Monseñor Gregorio Garavito Jiménez y el Padre José Otter, la Fundación se dedica a apoyar a los más pobres y desprotegidos.  En este sentido su compromiso de cristianos unidos forjando camino de esperanza en un solo mundo para ofrecer un futuro más humano a los más vulnerables, sobre todo los niños y niñas con y sin discapacidad.</p>
			<div class="Contenedor-images">
				<div class="Contenedor-images-int">
					<div class="Contenedor-images-int-sec">
						<img src="images/padre1.png">
						<h4>Mons. Gregorio Garavito Jimenez</h4>
						<p>(Q.E.P.D.)</p>
						<p>Nació en Junín Cundinamarca</p>
						<p>EL 9 de marzo de 1919</p>
						<p>Murió en Villavicencio (Meta)</p>
						<p>el 16 de febrero de 2016</p>
					</div>
					<div class="Contenedor-images-int-sec">
						<img src="images/padre2.png">
						<h4>Padre José Otter</h4>
						<p>(Q.E.P.D.)</p>
						<p>Nació en Haibach ( Alemania )</p>
						<p>EL 31 de marzo de 1944</p>
						<p>Murió en Mainaschaff (Alemania)</p>
						<p>el 12 de abril de 2006</p>
					</div>
					<div id="nosotros"></div>
				</div>

			</div>			
		</div>
	</div>
</section>

<section>
	<div class="Bloque1">
		<div class="Bloque1-int">
		<h4>QUIENES SOMOS</h4>

			<p>Institución católica, sin ánimo de lucro, bajo el lema “Un compromiso de Cristianos unidos en un solo mundo, para la formación y promoción de los más pobres y desprotegidos” (Preámbulo de los Estatutos), creada según Personería Jurídica No. 0815 del 29 de junio de 1993 emanada de la Gobernación del Departamento del Meta.</p>

			<h4>MISION</h4>

			<p>Trabajar desde la Nueva Evangelización en la Arquidiócesis de Villavicencio-Meta, acompañando la formación y promoción integral de los más pobres y desprotegidos.</p>

			<h4>VISION</h4>

			<p>Ser una Fundación líder en la formación integral de la persona humana basada en valores y principios cristianos católicos dirigida a niños, niñas y jóvenes con o sin diversidad funcional y sus familias, a través de la atención de un equipo humano con compromiso social.</p>

			<h4>OBJETIVOS</h4>

			<p>Comprometerse en la Nueva Evangelización trabajando por una formación y promoción integral de los más pobres y desprotegidos. </p>
			<p>Brindar una formación integral y atención basada en principios y valores cristianos a la población vulnerable de Primera Infancia, Infancia, Adolescencia, Juventud, Adulto Mayor y Familia. </p>
			<div id="programas"></div>
		</div>
	</div>

</section>
<section>
	<div class="Programas">
		<div class="Programas-int">
			<div class="Programas-int-sec">
				<h2>PROMOCION A LA FAMILIA</h2>
				<p>Promovemos la vida familiar de manera integral a 200 familias, destacando su valor como educadora y formadora de personas.  Fortaleciendo su economía familiar y su armonía con la obra de la creación.</p>
				<p>Dir. Cra. 47 A No. 11B  31 Barrio Esperanza 1ra. Etapa<br>
				Cel. 313 848 25 15</p>
			</div>

			<div class="Programas-int-sec">
				<h2>PRIMERA INFANCIA</h2>
				<p>Se presta el servicio integral a 250 niños y niñas de 2 a 4 años, en dos Sedes:</p>
				<p><strong>Centro Sagrada Familia: Dirección:</strong> Calle 52 sur No. 43-81<br>
				Tel. 669 54 18<br>
				Cel. 311 854 48 16</p>
				<p><strong>Sede San José - Reliquia:</strong> Dir. : mz. 76 casa 9 Barrio La Reliquia <br>
				Tel. 683 02 94<br>
				Cel. 312 523 21 31</p>       
			</div>

			<div class="Programas-int-sec">
				<h2>ALDEA ESTUDIANTIL CON MODALIDAD DE INTERNADO</h2>
				<p>Acogemos a 100 niños, niñas y jóvenes de los grados 8º a 11º y técnicos en producción agropecuaria con el SENA.</p>
				<p>Dir. Vereda Caney Medio – Cumaral (Meta)<br>
				Cel. 310 202 69 68</p>

			</div>


			<div class="Programas-int-sec">
				<h2>DIVERSIDAD FUNCIONAL</h2>
				<p>Atendemos a 160 niños, niñas y jóvenes con Diversidad Funcional, en dos Sedes:</p>
				<p><strong>Sede Centro La Sagrada Familia:</strong> Dir. Calle 52 sur No. 43- 81 Barrio Ciudad Porfía<br>
				Tel. 669 54 18<br>
				Cel. 311 484 46 73</p>
				<p><strong>Sede Centro Microempresarial P. José Otter:</strong> Dir. Calle 30 No. 26-56 Barrio Porvenir<br>
				Tel. 667 82 10<br>
				Cel. 314 393 67 00</p>				
			</div>

			<div class="Programas-int-sec">
				<h2>ESCUELA PARA TODOS</h2>
				<p>Acompañamiento psicosocial a 550 estudiantes de Primaria, Bachillerato, Técnicos y Tecnológicos, suministrándoles kit escolar, uniformes, pensión y transporte.</p>
				<p>Dir. Cra 47 A No. 11B  33 Barrio Esperanza 1ra. Etapa<br>
				Cel. 321 468 48 24</p>
				<div id="contactenos"></div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="Contactenos">
		<?php include_once("mod-correo.php") ?>
	</div>
</section>
<footer>
	<div class="Foot">
		<div class="Foot-int">
			<div class="Foot-int-sec">
				<h2>SEDE MONS. GREGORIO GARAVITO JIMENEZ</h2>
				<p>Dir. Calle 12 No. 47A 11 Barrio La Esperanza 1ra. Etapa<br>
				Tel. 667 44 35 – 668 33 36<br>
				Cel. 311 854 38 19</p>
				<h2>Sede San José - La Reliquia</h2>
				<p>Dir. Mz. 76 Casa 9 Barrio La Reliquia<br>
				Tel. 683 0294<br>
				Cel. 312 523 21 31</p>				
			</div>
			<div class="Foot-int-sec">
				<h2>Sede Centro de Capacitación Microempresarial Padre José Otter</h2>
				<p>Dir. Cll 30 N 26-56 Barrio Porvenir<br>
				Tel. 667 8210<br>
				Cel. 314 393 67 00</p>
				<h2>Sede Centro Sagrada Familia</h2>
				<p>Dir. Cll 52 Sur N 43-81 Barrio Ciudad Porfía<br>
				Tel. 669 54 18<br>
				Cel. 311 854 48 16</p>
			</div>
			<div class="Foot-int-sec">
				<h2>Aldea Estudiantil con modalidad de internado</h2>
				<p>Dir. Granja San José - Vereda Caney Medio - Cumaral Meta<br>
				Cel. 310 202 69 68</p>
			</div>
		</div>
		<div class="Foot-int Bord">
			<p>Copyright © Fundación Camino de la Esperanza, 2018. All rights reserved Designed by <a href="http://www.inngeniate.com" target="_blank">Inngeniate.com</a></p>
		</div>
	</div>
</footer>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/deslizar.js"></script>
<script src="js/menu-mini.js" type="text/javascript"></script>
<script src="js/sweetalert2.min.js"></script>
<script type="text/javascript" src="js/script_contacto.js"></script>
</body>
</html>
