<?php
include("conexion.php");

@$opc = $_REQUEST['opc'];
@$informacion = array();

///subir archivo
@$return = [];
@$informacion = [];
@$upload_folder ='../docs';
@$nombre_archivo = $_FILES['archivo']['name'];
@$tipo_archivo = $_FILES['archivo']['type'];
@$tamano_archivo = $_FILES['archivo']['size'];
@$tmp_archivo = $_FILES['archivo']['tmp_name'];
/////

@$nombre = $_POST['nombre'];

switch ($opc) {
	case 'crear':
		$nombre_archivo = limpiar($nombre_archivo);
		$archivador = $upload_folder . '/' . $nombre_archivo;
		if (!move_uploaded_file($tmp_archivo, $archivador)){
			$return = false;
		}
		else{
			$nombre = mysqli_real_escape_string($conexion,$nombre);
			$archivador = substr($archivador,3);
			$crea = mysqli_query($conexion,"INSERT INTO documentos VALUES('','$nombre','$archivador',NOW()) ");
			$return = true;
		}
		echo json_encode($return);
	break;
	case 'cargardocs':
		$bus = mysqli_query($conexion,"SELECT * FROM documentos ORDER BY id DESC");
		while($resp = mysqli_fetch_object($bus)){
			$informacion[] = $resp;
		}
		echo '{"documentos":'.json_encode($informacion).'}';
	break;
	case 'borrar':
		@$id = $_REQUEST['id'];
		$sel = mysqli_query($conexion,"SELECT * FROM documentos WHERE id='$id' ");
		$rs = mysqli_fetch_object($sel);
		unlink('../'.$rs->archivo);
		$eli  = mysqli_query($conexion,"DELETE FROM documentos WHERE id='$id' ");
		$informacion['status'] = true;
		echo json_encode($informacion);
	break;
}

function limpiar($String){
	$String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	$String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	$String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	$String = str_replace(array('í','ì','î','ï'),"i",$String);
	$String = str_replace(array('é','è','ê','ë'),"e",$String);
	$String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	$String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	$String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	$String = str_replace(array('ú','ù','û','ü'),"u",$String);
	$String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	$String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	$String = str_replace("ç","c",$String);
	$String = str_replace("Ç","C",$String);
	$String = str_replace("ñ","n",$String);
	$String = str_replace("Ñ","N",$String);
	$String = str_replace("Ý","Y",$String);
	$String = str_replace("ý","y",$String);
	$String = preg_replace('/\s+/', '_', $String);
	$String = str_replace(array( '(', ')' ), '', $String);
	return $String;
}

?>